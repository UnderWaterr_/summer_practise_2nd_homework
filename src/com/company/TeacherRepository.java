package com.company;
import com.company.models.Teacher;

import java.util.Optional;

public interface TeacherRepository {
    Optional<Teacher> findById(Long courseId);
}
