package com.company;

import com.company.models.Course;
import com.company.models.Student;
import com.company.models.Teacher;

import javax.sql.DataSource;
import java.io.FileReader;
import java.io.IOException;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;
import java.util.Properties;


public class Main {

    public static void main(String[] args) {
        Properties properties = new Properties();

        try {
            properties.load(new FileReader("resources\\application.properties"));
        } catch (IOException e) {
            throw new IllegalArgumentException(e);
        }

        DataSource dataSource = new HikariDataSourceWrapper(properties).getHikariDataSource();

        CoursesRepository coursesRepository = new CoursesRepositoryJdbcImpl(dataSource);

        List<Student> students = new ArrayList<>();
        students.add(new Student(1L));
        students.add(new Student(2L));
        students.add(new Student(3L));

        coursesRepository.save(
                new Course("Французский", LocalDate.parse("2020-02-05"), LocalDate.parse("2020-06-21"),
                        new Teacher(1L), students));

        System.out.println("findById: " + coursesRepository.findById(5L));

        System.out.println("findByName: " + coursesRepository.findByName("Французский").toString());

        System.out.println("findAll: " + coursesRepository.findAll().toString());

        coursesRepository.delete(5L);
    }
}
