package com.company;

import com.company.models.Course;

import java.util.List;
import java.util.Optional;

public interface CoursesRepository {
    void save(Course course);
    void update(Course course);
    void delete(Long courseId);
    Optional<Course>  findById(Long courseId);
    List<Course>  findByName(String courseName);
    List<Course> findAll();
}
