package com.company;

import com.company.models.Teacher;

import javax.sql.DataSource;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Optional;
import java.util.function.Function;

public class TeacherRepositoryJdbcImpl implements TeacherRepository {

    //language=SQL
    private final static String SQL_FIND_TEACHER_BY_ID = "select * from teacher where id = ?";
    DataSource dataSource;

    public TeacherRepositoryJdbcImpl(DataSource dataSource){
        this.dataSource = dataSource;
    }

    private final Function<ResultSet, Teacher> map = row -> {

        try {
            Long id = row.getLong("id");
            String firstName = row.getString("first_name");
            String secondName = row.getString("second_name");
            Integer workExperience = row.getInt("work_experience");

            return new Teacher(id, firstName, secondName, workExperience, new ArrayList<>());
        } catch (SQLException e) {
            throw new IllegalArgumentException(e);
        }

    };

    @Override
    public Optional<Teacher> findById(Long courseId) {
        ResultSet row = null;
        try (
                Connection connection = dataSource.getConnection();
                PreparedStatement statement = connection.prepareStatement(SQL_FIND_TEACHER_BY_ID)
        ){
            statement.setLong(1, courseId);

            row = statement.executeQuery();

            if (row.next()){
                return Optional.of(map.apply(row));
            }else{
                return Optional.empty();
            }

        } catch (SQLException e) {
            throw new IllegalArgumentException(e);
        }finally {
            if (row != null) {
                try {
                    row.close();
                } catch (SQLException ignored) {}
            }
        }
    }
}
