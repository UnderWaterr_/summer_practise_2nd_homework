package com.company;

import com.company.models.Course;
import com.company.models.Student;
import com.company.models.Teacher;

import javax.sql.DataSource;
import java.sql.*;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.function.Function;

public class CoursesRepositoryJdbcImpl implements CoursesRepository {
    //language=SQL
    private static final String SQL_SAVE_COURSE =
            "insert into course (name, start_date, end_date, teacher_id) values  (?, ?, ?, ?);";

    private static final String SQL_ADD_COURSE_TO_COURSE_TEACHERS_TABLE =
            "insert into course_teachers (id_teacher, id_course) VALUES (?, ?)";

    private static final String SQL_ADD_COURSE_TO_COURSE_STUDENTS_TABLE =
            "insert into course_students (id_student, id_course) VALUES (?, ?)";

    private static final String SQL_UPDATE_COURSE =
            "update course set name = ?, start_date = ?, end_date = ?, teacher_id = ? where id = ?";

    private static final String SQL_FIND_COURSE_BY_ID = "select * from course where id = ?";

    private static final String SQL_FIND_COURSE_BY_NAME = "select * from course where name = ? order by id";

    private static final String SQL_SELECT_ALL_COURSES = "select * from course order by id";

    private static final String SQL_DELETE_COURSE_FROM_COURSE_TEACHERS_TABLE =
            "delete from course_teachers where id_course = ?";

    private static final String SQL_DELETE_COURSE_FROM_COURSE_STUDENTS_TABLE =
            "delete from course_students where id_course = ?";

    private static final String SQL_DELETE_COURSE = "delete from course where id = ?";

    TeacherRepositoryJdbcImpl teacherRepositoryJdbc;

    DataSource dataSource;

    private final Function<ResultSet, Course> map = row -> {

        try {
            Long id = row.getLong("id");
            String name = row.getString("name");
            LocalDate startDate = LocalDate.parse(row.getString("start_date"));
            LocalDate endDate = LocalDate.parse(row.getString("end_date"));
            Teacher teacher = teacherRepositoryJdbc
                    .findById(row.getLong("teacher_id")).orElseThrow(IllegalAccessError::new);

            return new Course(id, name, startDate, endDate, teacher, new ArrayList<>());
        } catch (SQLException e) {
            throw new IllegalArgumentException(e);
        }

    };

    public CoursesRepositoryJdbcImpl(DataSource dataSource) {
        this.dataSource = dataSource;
        this.teacherRepositoryJdbc = new TeacherRepositoryJdbcImpl(dataSource);
    }

    @Override
    public void save(Course course) {
        ResultSet generatedKeys = null;
        try (Connection connection = dataSource.getConnection();
             PreparedStatement updateStatement =
                     connection.prepareStatement(SQL_SAVE_COURSE, Statement.RETURN_GENERATED_KEYS);
        ) {

            updateStatement.setString(1, course.getName());
            updateStatement.setString(2, course.getStartDate().toString());
            updateStatement.setString(3, course.getEndDate().toString());
            updateStatement.setLong(4, course.getTeacher().getId());

            if (updateStatement.executeUpdate() != 1) {
                throw new SQLException();
            }

            generatedKeys = updateStatement.getGeneratedKeys();
            if (generatedKeys.next()) {
                Long newId = generatedKeys.getLong(1);
                course.setId(newId);
                addCourseToCourseTeachersTable(newId, course.getTeacher().getId());
                addCourseToCourseStudentsTable(newId, course.getStudents());
            } else {
                throw new SQLException("Creating course failed, no ID obtained.");
            }

        } catch (SQLException e) {
            throw new IllegalArgumentException(e);
        } finally {
            if (generatedKeys != null) {
                try {
                    generatedKeys.close();
                } catch (SQLException ignored) {
                }
            }
        }
    }

    private void addCourseToCourseStudentsTable(Long courseId, List<Student> students) {
        try (
                Connection connection = dataSource.getConnection();
                PreparedStatement statement = connection.prepareStatement(SQL_ADD_COURSE_TO_COURSE_STUDENTS_TABLE);
        ) {
            for (Student student : students) {
                statement.setLong(1, student.getId());
                statement.setLong(2, courseId);

                if (statement.executeUpdate() != 1) {
                    throw new SQLException();

                }
            }

        } catch (SQLException e) {
            throw new IllegalArgumentException(e);
        }
    }


    private void addCourseToCourseTeachersTable(Long courseId, Long teacherId) {
        try (
                Connection connection = dataSource.getConnection();
                PreparedStatement statement = connection.prepareStatement(SQL_ADD_COURSE_TO_COURSE_TEACHERS_TABLE);
        ) {
            statement.setLong(1, teacherId);
            statement.setLong(2, courseId);

            if (statement.executeUpdate() != 1) {
                throw new SQLException();

            }
        } catch (SQLException e) {
            throw new IllegalArgumentException(e);
        }
    }

    @Override
    public void update(Course course) {
        try (
                Connection connection = dataSource.getConnection();
                PreparedStatement statement = connection.prepareStatement(SQL_UPDATE_COURSE);
        ) {
            statement.setString(1, course.getName());
            statement.setString(2, course.getStartDate().toString());
            statement.setString(3, course.getEndDate().toString());
            statement.setLong(4, course.getTeacher().getId());
            statement.setLong(5, course.getId());

            int affectedRows = statement.executeUpdate();

            if (affectedRows != 1) {
                throw new SQLException("Exception in <Update>");
            }

        } catch (SQLException e) {
            throw new IllegalArgumentException(e);
        }
    }


    @Override
    public Optional<Course> findById(Long courseId) {
        ResultSet row = null;
        try (
                Connection connection = dataSource.getConnection();
                PreparedStatement statement = connection.prepareStatement(SQL_FIND_COURSE_BY_ID)
        ) {
            statement.setLong(1, courseId);

            row = statement.executeQuery();

            if (row.next()) {
                return Optional.of(map.apply(row));
            } else {
                return Optional.empty();
            }

        } catch (SQLException e) {
            throw new IllegalArgumentException(e);
        } finally {
            if (row != null) {
                try {
                    row.close();
                } catch (SQLException ignored) {
                }
            }
        }
    }


    @Override
    public List<Course> findByName(String courseName) {
        ResultSet row = null;
        try (
                Connection connection = dataSource.getConnection();
                PreparedStatement statement = connection.prepareStatement(SQL_FIND_COURSE_BY_NAME)
        ) {
            List<Course> courses = new ArrayList<>();
            statement.setString(1, courseName);

            row = statement.executeQuery();

            while (row.next()) {
                courses.add(map.apply(row));
            }
            return courses;
        } catch (SQLException e) {
            throw new IllegalArgumentException(e);
        } finally {
            if (row != null) {
                try {
                    row.close();
                } catch (SQLException ignored) {
                }
            }
        }
    }

    @Override
    public List<Course> findAll() {
        ResultSet row = null;
        try (
                Connection connection = dataSource.getConnection();
                Statement statement = connection.createStatement();
        ) {
            List<Course> courses = new ArrayList<>();
            row = statement.executeQuery(SQL_SELECT_ALL_COURSES);

            while (row.next()) {
                courses.add(map.apply(row));
            }
            return courses;
        } catch (SQLException e) {
            throw new IllegalArgumentException(e);
        } finally {
            if (row != null) {
                try {
                    row.close();
                } catch (SQLException ignored) {
                }
            }
        }
    }

    @Override
    public void delete(Long courseId) {

        try (Connection connection = dataSource.getConnection();
             PreparedStatement statement =
                     connection.prepareStatement(SQL_DELETE_COURSE);
        ) {
            statement.setLong(1, courseId);
            deleteCourseFromCourseTeachersTable(courseId);
            deleteCourseFromCoursesStudentsTable(courseId);
            statement.execute();

        } catch (SQLException e) {
            throw new IllegalArgumentException(e);
        }
    }

    private void deleteCourseFromCourseTeachersTable(Long courseId) {
        try (
                Connection connection = dataSource.getConnection();
                PreparedStatement statement = connection.prepareStatement(SQL_DELETE_COURSE_FROM_COURSE_TEACHERS_TABLE);
        ) {
            statement.setLong(1, courseId);

            statement.execute();

        } catch (SQLException e) {
            throw new IllegalArgumentException(e);
        }
    }

    private void deleteCourseFromCoursesStudentsTable(Long courseId) {
        try (
                Connection connection = dataSource.getConnection();
                PreparedStatement statement = connection.prepareStatement(SQL_DELETE_COURSE_FROM_COURSE_STUDENTS_TABLE);
        ) {
            statement.setLong(1, courseId);

            statement.execute();
        } catch (SQLException e) {
            throw new IllegalArgumentException(e);
        }
    }
}
